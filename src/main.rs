#![warn(clippy::all, clippy::pedantic, clippy::nursery)]

use ramp::Int;
use std::collections::HashSet;

fn power_when_all_digits_seen(n: &Int) -> Int {
    let mut n = n.clone();

    let base: Int = Int::from(10);

    if n == 0 {
        return Int::zero();
    }

    // f(200) = f(20) = f(2) etc.
    while &n % &base == 0 {
        n /= &base;
    }

    let n = n;

    // Cases that will never terminate
    if n == 1 {
        return Int::zero();
    }

    let mut unseen_digits: HashSet<Int> = (0..=9).into_iter().map(|x| Int::from(x)).collect();
    let mut exponent = Int::from(0);
    let mut n_pow = Int::from(1);

    while !unseen_digits.is_empty() {
        n_pow *= &n;
        exponent += 1;

        // Loop through digits
        let mut n_red = n_pow.clone();
        while n_red != 0 {
            let d = &n_red % &base;
            unseen_digits.remove(&d);
            n_red /= &base;
        }
    }

    exponent
}

fn main() {
    let mut i = Int::zero();
    loop {
        let pow = power_when_all_digits_seen(&i);
        println!("{}\t{}", i, pow);

        i += 1;
    }
}
